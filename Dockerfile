FROM ubuntu:latest

# Run this before setting ENV so that ENV changes don't break docker cache
# buildah and skopeo don't have pre-built downloads outside of packages, so we will install
# them via apt
# gettext-base is added to provide envsubst
RUN apt-get update -qq && \
    apt-get install -qq -y software-properties-common && \
    apt-add-repository -y ppa:projectatomic/ppa && \
    apt-get update -qq && \
    apt-get install -y --no-install-recommends curl ca-certificates buildah skopeo openssh-client gettext-base git && \
    rm -rf /var/lib/apt/lists/*


ENV CURL_CMD="curl -f -L -y 1 --retry 5"

# Install docker CLI (the RPM dependencies aren't worth the RPM install)
ENV DOCKER_VERSION=18.09.9
RUN ${CURL_CMD} -o /tmp/docker.tgz https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz && \
    tar -xvzf /tmp/docker.tgz -C /usr/local/bin --strip=1 docker/docker && \
    rm -f /tmp/docker.tgz

ENV HELM_ARCH=linux-amd64

ENV HELM3_VERSION=3.0.2
RUN ${CURL_CMD} -o /tmp/helm.tgz https://get.helm.sh/helm-v${HELM3_VERSION}-${HELM_ARCH}.tar.gz && \
    tar xvzf /tmp/helm.tgz -C /usr/local/bin --strip=1 ${HELM_ARCH}/helm && \
    mv /usr/local/bin/helm /usr/local/bin/helm3 &&\
    rm -f /tmp/helm.tgz

# Install helm
ENV HELM2_VERSION=2.16.1
RUN ${CURL_CMD} -o /tmp/helm.tgz https://get.helm.sh/helm-v${HELM2_VERSION}-${HELM_ARCH}.tar.gz && \
    tar xvzf /tmp/helm.tgz -C /usr/local/bin --strip=1 ${HELM_ARCH}/helm ${HELM_ARCH}/tiller && \
    mv /usr/local/bin/helm /usr/local/bin/helm2 && \
    ln -s /usr/local/bin/helm2 /usr/local/bin/helm && \
    rm -f /tmp/helm.tgz

# Install oc
ENV OC_VERSION=3.11.0
ENV OC_COMMIT_SHA=0cbc58b
ENV OC_ARCH=linux-64bit
RUN ${CURL_CMD} -o /tmp/oc.tgz --max-time 35 https://github.com/openshift/origin/releases/download/v${OC_VERSION}/openshift-origin-client-tools-v${OC_VERSION}-${OC_COMMIT_SHA}-${OC_ARCH}.tar.gz && \
    tar -xvzf /tmp/oc.tgz -C /usr/local/bin --strip=1 openshift-origin-client-tools-v${OC_VERSION}-${OC_COMMIT_SHA}-${OC_ARCH}/oc && \
    rm -f /tmp/oc.tgz

# Install kubectl
ENV KUBECTL_VERSION=1.15.3
RUN ${CURL_CMD} -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod 755 /usr/local/bin/kubectl
